package com.atlassian.pocketknife.api.querydsl.stream;

import com.atlassian.pocketknife.internal.querydsl.stream.PartitionedCloseableIterable;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CloseableIterablesTest {

    @Mock
    private CloseableIterable<Object> closeableIterable1;

    @Mock
    private CloseableIterable<Object> closeableIterable2;

    @Test
    public void partitioned__empty_list_no_values() {
        final CloseableIterable<Object> partitioned = CloseableIterables.partitioned(emptyList());

        assertThat(partitioned.fetchFirst(), is(Optional.empty()));
    }

    @Test
    public void partitioned__null_treated_as_empty() {
        final CloseableIterable<Object> partitioned = CloseableIterables.partitioned(null);

        assertThat(partitioned.fetchFirst(), is(Optional.empty()));
    }

    @Test(expected = NullPointerException.class)
    public void partitioned__exception_if_null_in_list() {
        CloseableIterables.partitioned(singletonList(null));
    }

    @Test
    public void partitioned__returns_partioned_closeable_iterator_single_iterable() {
        final CloseableIterable<Object> partitioned = CloseableIterables.partitioned(ImmutableList.of(closeableIterable1));

        assertThat(partitioned, instanceOf(PartitionedCloseableIterable.class));
    }

    @Test
    public void partitioned__returns_partioned_closeable_iterator_multiple_iterables() {
        final CloseableIterable<Object> partitioned = CloseableIterables.partitioned(ImmutableList.of(closeableIterable1, closeableIterable2));

        assertThat(partitioned, instanceOf(PartitionedCloseableIterable.class));
    }

}