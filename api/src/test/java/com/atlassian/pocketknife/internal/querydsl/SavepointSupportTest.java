package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.internal.querydsl.mocks.TestingContext;
import com.atlassian.pocketknife.internal.querydsl.util.Unit;
import com.querydsl.core.Tuple;
import org.junit.Before;
import org.junit.Test;

import java.sql.Savepoint;
import java.util.List;

import static com.atlassian.pocketknife.api.querydsl.util.OnRollback.NOOP;
import static com.atlassian.pocketknife.internal.querydsl.tables.domain.QEmployee.employee;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Shows how our save point support works
 */
public class SavepointSupportTest {

    private DatabaseAccessor databaseAccessor;

    @Before
    public void setUp() throws Exception {

        TestingContext testingContext = TestingContext.forH2();

        databaseAccessor = testingContext.getDatabaseAccessor();
    }

    @Test
    public void testSavepointSupport() throws Exception {
        databaseAccessor.runInNewTransaction(dbConn -> {
            List<Tuple> data = getExpectedEmployees(dbConn, 10);

            Savepoint firstSP = dbConn.setSavepoint();
            for (int i = 0; i < 5; i++) {
                Integer id = data.get(i).get(employee.id);
                dbConn.delete(employee).where(employee.id.eq(id)).execute();
            }
            data = getExpectedEmployees(dbConn, 5);

            Savepoint secondSP = dbConn.setSavepoint("second");

            // the 2 managers in the dataset should be left
            dbConn.delete(employee).where(employee.superiorId.ne(-1)).execute();

            data = getExpectedEmployees(dbConn, 2);

            dbConn.rollback(secondSP);

            data = getExpectedEmployees(dbConn, 5);

            dbConn.rollback(firstSP);
            data = getExpectedEmployees(dbConn, 10);

            return Unit.VALUE;
        }, NOOP);
    }

    private List<Tuple> getExpectedEmployees(DatabaseConnection dbConn, int expectedCount) {
        List<Tuple> data;
        data = dbConn.select(employee.id, employee.firstname, employee.lastname)
                .from(employee)
                .orderBy(employee.id.desc())
                .fetch();
        assertThat(data.size(), equalTo(expectedCount));
        return data;
    }

}
