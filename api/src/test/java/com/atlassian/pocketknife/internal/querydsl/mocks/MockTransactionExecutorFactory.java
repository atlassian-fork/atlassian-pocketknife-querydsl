package com.atlassian.pocketknife.internal.querydsl.mocks;

import com.atlassian.pocketknife.internal.querydsl.tables.TestConnections;
import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.api.rdbms.RdbmsException;
import com.atlassian.sal.api.rdbms.TransactionalExecutor;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import com.atlassian.sal.core.rdbms.DefaultTransactionalExecutor;
import com.atlassian.sal.spi.HostConnectionAccessor;
import io.atlassian.fugue.Option;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.SQLException;

public class MockTransactionExecutorFactory implements TransactionalExecutorFactory
{
    private final Option<String> schemName;

    public MockTransactionExecutorFactory() {
        this(Option.some("PUBLIC"));
    }

    public MockTransactionExecutorFactory(Option<String> schemName) {
        this.schemName = schemName;
    }

    @Override
    public TransactionalExecutor createExecutor()
    {
        return createExecutor(false, false);
    }

    @Override
    public TransactionalExecutor createExecutor(final boolean readOnly, final boolean requiresNew)
    {
        return new DefaultTransactionalExecutor(new MockHostConnectionAccessor(), readOnly, requiresNew);
    }

    private class MockHostConnectionAccessor implements HostConnectionAccessor
    {

        @Override
        public <A> A execute(final boolean b, final boolean b2, @Nonnull final ConnectionCallback<A> callback)
        {
            Connection connection = TestConnections.getConnection();
            Boolean autoCommit = getAutoCommit(connection);
            setAutoCommit(connection, false);
            try
            {
                return callback.execute(connection);
            }
            finally
            {
                setAutoCommit(connection, autoCommit);
            }
        }

        @Nonnull
        @Override
        public Option<String> getSchemaName()
        {
            return schemName;
        }

        private void setAutoCommit(final Connection connection, boolean autocomit)
        {
            try
            {
                connection.setAutoCommit(autocomit);
            }
            catch (final SQLException e)
            {
                throw new RdbmsException("unable to invoke java.sql.Connection#getAutoCommit", e);
            }
        }

        private boolean getAutoCommit(final Connection connection)
        {
            try
            {
                return connection.getAutoCommit();
            }
            catch (final SQLException e)
            {
                throw new RdbmsException("unable to invoke java.sql.Connection#setAutoCommit", e);
            }
        }
    }
}
