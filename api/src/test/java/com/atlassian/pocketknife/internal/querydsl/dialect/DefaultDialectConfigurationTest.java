package com.atlassian.pocketknife.internal.querydsl.dialect;

import com.atlassian.pocketknife.api.querydsl.configuration.ConfigurationEnrichment;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.atlassian.pocketknife.internal.querydsl.configuration.ConfigurationEnrichmentImpl;
import com.atlassian.pocketknife.internal.querydsl.mocks.MockConfigurationEnricher;
import com.atlassian.pocketknife.internal.querydsl.mocks.MockSchemaProvider;
import com.querydsl.sql.HSQLDBTemplates;
import com.querydsl.sql.MySQLTemplates;
import com.querydsl.sql.OracleTemplates;
import com.querydsl.sql.PostgreSQLTemplates;
import com.querydsl.sql.SQLServer2005Templates;
import com.querydsl.sql.SQLServerTemplates;
import com.querydsl.sql.SQLTemplates;
import io.atlassian.fugue.Pair;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultDialectConfigurationTest {

    private DefaultDialectConfiguration classUnderTest;
    private DatabaseMetaData databaseMetaData;
    private Connection connection;
    private ConfigurationEnrichment configurationEnrichment;
    private MockConfigurationEnricher configurationEnricher;

    @Before
    public void setUp() throws Exception {
        classUnderTest = null;
        connection = mock(Connection.class);
        databaseMetaData = mock(DatabaseMetaData.class);
        configurationEnricher = new MockConfigurationEnricher();
        configurationEnrichment = new ConfigurationEnrichmentImpl();
        configurationEnrichment.setEnricher(configurationEnricher);

        when(connection.getMetaData()).thenReturn(databaseMetaData);
    }

    /**
     * As we build out our database support we should expand this class
     */
    @Test
    public void testDatabaseDetectionCoverage() throws Exception {
        Map<String, Class> support = new LinkedHashMap<>();
        support.put(":postgresql:", PostgreSQLTemplates.Builder.class);
        support.put(":oracle:", OracleTemplates.Builder.class);
        support.put(":hsqldb:", HSQLDBTemplates.Builder.class);
        support.put(":sqlserver:", SQLServerTemplates.Builder.class);
        support.put(":mysql:", MySQLTemplates.Builder.class);

        for (String connStr : support.keySet()) {
            assertDatabaseType(support.get(connStr), connStr);
        }
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testDatabaseDetectionUnknownDB() throws Exception {
        assertDatabaseType(OracleTemplates.Builder.class, "SomeDatabaseWeDon'tSupport");
    }

    @Test
    public void testDatabaseDetectionRecoversFromTransientErrors() throws Exception {
        final SQLException sqle = new SQLException("Just testing!");
        when(connection.getMetaData())
                .thenThrow(sqle)
                .thenReturn(databaseMetaData);

        classUnderTest = makeDialectConfig(PostgreSQLTemplates.Builder.class, ":postgresql:");
        try {
            classUnderTest.getDialectConfig(connection);
            fail("Expected a LazyReference.InitializationException");
        } catch (RuntimeException ie) {
            assertEquals(sqle, ie.getCause());
        }

        when(connection.getMetaData()).thenReturn(databaseMetaData);
        assertThat(classUnderTest.getDialectConfig(connection), notNullValue());
    }

    @Test
    public void testDatabaseDetectionForSideEffect() throws Exception {
        classUnderTest = assertDatabaseType(PostgreSQLTemplates.Builder.class, ":postgresql:");
        assertThat(classUnderTest.getDialectConfig(connection), notNullValue());
    }

    @Test
    public void test_get_db_template_for_sqlserver_2005() throws SQLException {
        String sqlServerConnStr = "jdbc:jtds:sqlserver://dbserver:1433/schema";
        Class<SQLServer2005Templates> expectedSQLServerTemplate = SQLServer2005Templates.class;

        when(databaseMetaData.getDatabaseMajorVersion()).thenReturn(DialectHelper.SQLSERVER_2005);

        classUnderTest = assertDatabaseType(SQLServer2005Templates.builder().getClass(), sqlServerConnStr);

        Pair<SQLTemplates.Builder, DialectProvider.SupportedDatabase> dbTemplate = classUnderTest.getDBTemplate(sqlServerConnStr, databaseMetaData);
        assertThat(dbTemplate, notNullValue());
        assertThat(dbTemplate.left().build(), instanceOf(expectedSQLServerTemplate));
        assertThat(dbTemplate.right(), instanceOf(DialectProvider.SupportedDatabase.SQLSERVER.getClass()));
    }

    @Test
    public void test_get_db_template_for_non_sqlserver() throws SQLException {
        String serverConnStr = "jdbc:mysql://dbserver:3306/schema";
        Class<MySQLTemplates> expectedServerTemplate = MySQLTemplates.class;

        classUnderTest = assertDatabaseType(MySQLTemplates.builder().getClass(), serverConnStr);

        Pair<SQLTemplates.Builder, DialectProvider.SupportedDatabase> dbTemplate = classUnderTest.getDBTemplate(serverConnStr, databaseMetaData);
        assertThat(dbTemplate, notNullValue());
        assertThat(dbTemplate.left().build(), instanceOf(expectedServerTemplate));
        assertThat(dbTemplate.right(), instanceOf(DialectProvider.SupportedDatabase.MYSQL.getClass()));
    }

    @Test
    public void test_side_effects_called() throws Exception {
        // assemble
        when(databaseMetaData.getURL()).thenReturn("jdbc:mysql://dbserver:3306/schema");
        classUnderTest = new DefaultDialectConfiguration(new MockSchemaProvider(), configurationEnrichment);

        // act
        classUnderTest.getDialectConfig(connection);
        classUnderTest.getDialectConfig(connection);

        assertThat(configurationEnricher.enrichCount, equalTo(2));
    }

    @Test
    public void testSchemaNotPrintedWhenNotProvided() throws SQLException {
        when(databaseMetaData.getURL()).thenReturn("jdbc:jtds:sqlserver://dbserver:1433/dbname");
        when(databaseMetaData.getDatabaseMajorVersion()).thenReturn(DialectHelper.SQLSERVER_2012);

        classUnderTest = new DefaultDialectConfiguration(new MockSchemaProvider(Optional.empty()), configurationEnrichment);

        assertThat(classUnderTest.getDialectConfig(connection).getSqlTemplates().isPrintSchema(), is(false));
    }

    @Test
    public void testSchemaPrintedWhenProvided() throws SQLException {
        when(databaseMetaData.getURL()).thenReturn("jdbc:jtds:sqlserver://dbserver:1433/dbname");
        when(databaseMetaData.getDatabaseMajorVersion()).thenReturn(DialectHelper.SQLSERVER_2012);

        classUnderTest = new DefaultDialectConfiguration(new MockSchemaProvider(Optional.of("dbo")), configurationEnrichment);

        assertThat(classUnderTest.getDialectConfig(connection).getSqlTemplates().isPrintSchema(), is(true));
    }


    private DefaultDialectConfiguration assertDatabaseType(final Class builderClass, String connStr)
            throws SQLException {
        DefaultDialectConfiguration dialectConfiguration = makeDialectConfig(builderClass, connStr);
        DialectProvider.Config config = dialectConfiguration.getDialectConfig(connection);

        assertThat(config.getSqlTemplates().isUseQuotes(), equalTo(true));
        return dialectConfiguration;
    }

    private DefaultDialectConfiguration makeDialectConfig(final Class builderClass, String connStr) throws SQLException {
        DefaultDialectConfiguration dialectConfiguration = new DefaultDialectConfiguration(new MockSchemaProvider(), configurationEnrichment) {
            @Override
            public SQLTemplates.Builder enrich(final SQLTemplates.Builder builder) {
                assertThat(builder, instanceOf(builderClass));
                return super.enrich(builder);
            }
        };
        when(databaseMetaData.getURL()).thenReturn(connStr);
        return dialectConfiguration;
    }
}