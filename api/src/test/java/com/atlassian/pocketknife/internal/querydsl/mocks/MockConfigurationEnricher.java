package com.atlassian.pocketknife.internal.querydsl.mocks;

import com.atlassian.pocketknife.spi.querydsl.configuration.ConfigurationEnricher;
import com.querydsl.sql.Configuration;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class MockConfigurationEnricher implements ConfigurationEnricher {
    public int enrichCount = 0;

    @Override
    public Configuration enrich(Configuration configuration) {
        enrichCount++;
        return configuration;
    }
}
