package com.atlassian.pocketknife.internal.querydsl.mocks;

import com.atlassian.pocketknife.api.querydsl.configuration.ConfigurationEnrichment;
import com.atlassian.pocketknife.internal.querydsl.dialect.DefaultDialectConfiguration;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaProvider;
import com.querydsl.sql.Configuration;
import com.querydsl.sql.HSQLDBTemplates;
import com.querydsl.sql.SQLTemplates;

import java.sql.Connection;

public class HsqlDialectConfiguration extends DefaultDialectConfiguration {

    private final DebuggingDetailedListener detailedListener;

    public HsqlDialectConfiguration(SchemaProvider schemaProvider, ConfigurationEnrichment enrichment, DebuggingDetailedListener detailedListener) {
        super(schemaProvider, enrichment);
        this.detailedListener = detailedListener;
    }

    @Override
    public Config getDialectConfig(final Connection connection) {
        SQLTemplates.Builder builder = HSQLDBTemplates.builder();
        enrich(builder);
        SQLTemplates sqlTemplates = builder.build();

        DatabaseInfo databaseInfo = new DatabaseInfo(SupportedDatabase.HSQLDB, "HSQL", "Version 0.0", 0, 0, "HSQL Driver", 0, 0);

        Configuration configuration = new Configuration(sqlTemplates);
        enrich(configuration);
        configuration.addListener(detailedListener);

        return new Config(sqlTemplates, configuration, databaseInfo);
    }

}

