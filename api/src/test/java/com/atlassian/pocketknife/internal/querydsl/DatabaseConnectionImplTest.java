package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.atlassian.pocketknife.internal.querydsl.dialect.DialectConfiguration;
import com.atlassian.pocketknife.internal.querydsl.mocks.MockConnection;
import com.atlassian.pocketknife.internal.querydsl.mocks.TestingContext;
import com.atlassian.pocketknife.internal.querydsl.tables.TestConnections;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QEmployee;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class DatabaseConnectionImplTest {
    private DialectConfiguration dialectConfiguration;
    private DialectProvider.Config dialectConfig;
    private Connection jdbcConnection;

    @Before
    public void setUp() throws Exception {
        dialectConfiguration = TestingContext.forH2().getDialectConfiguration();
        dialectConfig = dialectConfiguration.getDialectConfig(null);

        jdbcConnection = new SpecificBehaviourConnection(TestConnections.getConnection());
        jdbcConnection.setAutoCommit(false);
    }

    @Test
    public void test_externally_managed() throws Exception {
        DatabaseConnectionImpl managedConnection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, true);

        assertThat(managedConnection.isAutoCommit(), equalTo(false));
        assertThat(managedConnection.isInTransaction(), equalTo(true));
        assertThat(managedConnection.isExternallyManaged(), equalTo(true));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_cant_call_commit() throws Exception {
        DatabaseConnectionImpl managedConnection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, true);
        managedConnection.commit();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_cant_call_rollback() throws Exception {
        DatabaseConnectionImpl managedConnection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, true);
        managedConnection.rollback();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_cant_call_set_autocommit() throws Exception {
        DatabaseConnectionImpl managedConnection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, true);
        managedConnection.setAutoCommit(true);
    }

    @Test
    public void test_can_call_commit() throws Exception {
        DatabaseConnectionImpl managedConnection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, false);
        managedConnection.commit();
    }

    @Test
    public void test_can_call_rollback() throws Exception {
        DatabaseConnectionImpl managedConnection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, false);
        managedConnection.rollback();
    }

    @Test
    public void test_can_call_set_autocommit() throws Exception {
        DatabaseConnectionImpl managedConnection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, false);
        managedConnection.setAutoCommit(true);
    }

    @Test
    public void test_can_always_call_set_savepoint_named() throws Exception {
        DatabaseConnectionImpl connection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, false);
        assertThat(connection.setSavepoint("unManaged"), Matchers.notNullValue());

        connection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, true);
        assertThat(connection.setSavepoint("Managed"), Matchers.notNullValue());
    }

    @Test
    public void test_can_always_call_set_savepoint() throws Exception {
        DatabaseConnectionImpl connection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, false);
        assertThat(connection.setSavepoint(), Matchers.notNullValue());

        connection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, true);
        assertThat(connection.setSavepoint(), Matchers.notNullValue());
    }

    @Test
    public void test_can_always_call_rollback_savepoint() throws Exception {
        DatabaseConnectionImpl connection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, false);
        Savepoint savepoint = connection.setSavepoint();
        connection.rollback(savepoint);

        connection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, true);
        savepoint = connection.setSavepoint();
        connection.rollback(savepoint);
    }

    @Test
    public void test_can_always_call_release_savepoint() throws Exception {
        DatabaseConnectionImpl connection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, false);
        Savepoint savepoint = connection.setSavepoint();
        connection.releaseSavepoint(savepoint);

        connection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, true);
        savepoint = connection.setSavepoint();
        connection.releaseSavepoint(savepoint);
    }

    @Test
    public void test_basic_non_null_checks() throws Exception {
        DatabaseConnectionImpl connection = new DatabaseConnectionImpl(dialectConfig, jdbcConnection, false);
        assertThat(connection.select(QEmployee.employee.id), Matchers.notNullValue());
        assertThat(connection.insert(QEmployee.employee), Matchers.notNullValue());
        assertThat(connection.update(QEmployee.employee), Matchers.notNullValue());
        assertThat(connection.delete(QEmployee.employee), Matchers.notNullValue());
        assertThat(connection.query(), Matchers.notNullValue());
    }

    @Test
    public void test_close() throws Exception {
        AtomicInteger closedCount = new AtomicInteger();
        MockConnection mockConnection = new MockConnection(jdbcConnection) {
            @Override
            public void close() throws SQLException {
                super.close();
                closedCount.incrementAndGet();
            }
        };
        DatabaseConnectionImpl connection = new DatabaseConnectionImpl(dialectConfig, mockConnection, false);
        connection.close();
        assertThat(mockConnection.isClosed(), equalTo(true));
        assertThat(closedCount.get(), equalTo(1));
        connection.close();
        assertThat(mockConnection.isClosed(), equalTo(true));
        assertThat(closedCount.get(), equalTo(1));

    }
}