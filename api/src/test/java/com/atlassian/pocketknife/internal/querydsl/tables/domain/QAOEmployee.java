package com.atlassian.pocketknife.internal.querydsl.tables.domain;

import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.core.types.dsl.TimePath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.RelationalPathBase;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

public class QAOEmployee extends RelationalPathBase<Employee> {

    public static final QAOEmployee employee = new QAOEmployee("AO_Employee");  // mixed case to be pathological for alias

    private static final long serialVersionUID = 1394463749655231079L;

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath firstname = createString("firstName");

    public final StringPath lastname = createString("LastName");

    public final NumberPath<BigDecimal> salary = createNumber("salary", BigDecimal.class);

    public final DatePath<Date> datefield = createDate("datefield", Date.class);

    public final TimePath<Time> timefield = createTime("timefield", Time.class);

    public final NumberPath<Integer> superiorId = createNumber("superior_Id", Integer.class);


    public QAOEmployee(String path) {
        super(Employee.class, PathMetadataFactory.forVariable(path), "PUBLIC", "AO_Employee");
        addMetadata();
    }

    protected void addMetadata() {
        // mixed case - not good but possible
        addMetadata(id, ColumnMetadata.named("ID"));
        addMetadata(firstname, ColumnMetadata.named("firstName"));
        addMetadata(lastname, ColumnMetadata.named("LastName"));
        addMetadata(salary, ColumnMetadata.named("SALARY"));
        addMetadata(datefield, ColumnMetadata.named("DATEFIELD"));
        addMetadata(timefield, ColumnMetadata.named("TIMEFIELD"));
        addMetadata(superiorId, ColumnMetadata.named("SUPERIOR_ID"));
    }

}
