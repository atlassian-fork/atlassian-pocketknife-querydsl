package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.stream.ClosePromise;
import com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable;
import com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory;
import com.atlassian.pocketknife.internal.querydsl.mocks.TestingContext;
import com.atlassian.pocketknife.internal.querydsl.stream.StreamingQueryFactoryImpl;
import com.atlassian.pocketknife.internal.querydsl.util.Unit;
import com.mysema.commons.lang.CloseableIterator;
import com.querydsl.core.Tuple;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.atlassian.pocketknife.api.querydsl.util.OnRollback.NOOP;
import static com.atlassian.pocketknife.internal.querydsl.tables.domain.QEmployee.employee;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Tests the 'quasi-functional' aspects of CloseableIterables
 */
public class IterablesFunctionalTest {
    public static final Function<Tuple, String> EMPLOYEE_FIRST_NAME_MAPPER = input -> input.get(employee.firstname);
    public static final Predicate<String> CONTAINS_M = input -> input.contains("M");

    private static final int TOTAL_NUMBER_EMPLOYEES_IN_DATASET = 10;

    private StreamingQueryFactory streamyQueryFactory;
    private DatabaseAccessor databaseQuery;

    @Before
    public void setUp() throws Exception {
        databaseQuery = TestingContext.forH2().getDatabaseAccessor();
        streamyQueryFactory = new StreamingQueryFactoryImpl();

    }

    @Test
    public void testStreamyFetchFirst() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            Optional<Tuple> firstTuple = streamyResult.fetchFirst();
            assertThat(firstTuple.isPresent(), equalTo(true));

            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyMapExpression() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            Optional<String> firstName = streamyResult.map(employee.firstname).fetchFirst();
            assertThat(firstName.isPresent(), equalTo(true));
            assertThat(firstName.get(), equalTo("Mike"));
            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyTake() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            CloseableIterable<Tuple> taken = streamyResult.take(2);
            assertThat(consumerAndCount(taken), equalTo(2));

            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyTakeWhile() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            CloseableIterable<String> taken = streamyResult.map(EMPLOYEE_FIRST_NAME_MAPPER).takeWhile(CONTAINS_M);
            assertThat(consumerAndCount(taken), equalTo(2));

            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyTakeWhenNotEnough() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            CloseableIterable<Tuple> taken = streamyResult.take(2000);
            assertThat(consumerAndCount(taken), equalTo(10));


            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyMapWithTake() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            CloseableIterable<String> taken = streamyResult.map(EMPLOYEE_FIRST_NAME_MAPPER).take(2);
            assertThat(consumerAndCount(taken), equalTo(2));

            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyMapWithTakeZero() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            Function<Tuple, String> mapper = input -> input.get(employee.firstname);
            CloseableIterable<String> taken = streamyResult.map(mapper).take(0);
            assertThat(consumerAndCount(taken), equalTo(0));

            return Unit.VALUE;
        }, NOOP);

    }

    @Test
    public void testStreamyTakeWithMap() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            Function<Tuple, String> mapper = input -> input.get(employee.firstname);
            CloseableIterable<String> taken = streamyResult.take(2).map(mapper);
            assertThat(consumerAndCount(taken), equalTo(2));

            return Unit.VALUE;
        }, NOOP);
    }

    @Test(expected = IllegalStateException.class)
    public void testStreamyTakeChecksForAlreadyTaken() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            CloseableIterable<Tuple> taken = streamyResult.take(10);
            consume(taken, 3);
            taken.take(2);

            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyMapWithMap() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            Function<Tuple, String> mapper1 = input -> input.get(employee.firstname);
            Function<String, Integer> mapper2 = String::length;
            Optional<Integer> firstLength = streamyResult.map(mapper1).map(mapper2).fetchFirst();
            assertThat(firstLength.isPresent(), equalTo(true));
            assertThat(firstLength.get(), equalTo("Mike".length()));

            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyFilter() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            Predicate<String> notContainsM = CONTAINS_M.negate();

            CloseableIterable<String> taken = streamyResult.map(EMPLOYEE_FIRST_NAME_MAPPER).filter(notContainsM);
            assertThat(consumerAndCount(taken), equalTo(10 - 2));

            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyFilterWithAFilter() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            Predicate<String> containsJ = input -> input.contains("J");
            Predicate<String> notContainsM = CONTAINS_M.negate();

            CloseableIterable<String> taken = streamyResult.map(EMPLOYEE_FIRST_NAME_MAPPER).filter(notContainsM).filter(containsJ);
            assertThat(consumerAndCount(taken), equalTo(3)); // Joe, Jim and Jennifer

            return Unit.VALUE;
        }, NOOP);
    }


    @Test
    public void testStreamyForEachOnTuples() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            final List<Tuple> tuples = new ArrayList<>();

            try (CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn)) {
                streamyResult.foreach(tuples::add);
            }
            assertThat(tuples.size(), equalTo(10));

            return Unit.VALUE;
        }, NOOP);
    }

    @Test
    public void testStreamyForEachAfterMapping() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            final List<String> firstNames = new ArrayList<>();

            try (CloseableIterable<String> streamyResult = streamEmployees(dbConn).map(employee.firstname)) {
                streamyResult.foreach(firstNames::add);
            }
            assertThat(firstNames.size(), equalTo(10));

            return Unit.VALUE;
        }, NOOP);
    }


    @Test(expected = IllegalStateException.class)
    public void testStreamyFetchFirstThrowsExceptionWhenCalledTwice() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            streamyResult.fetchFirst();
            streamyResult.fetchFirst();

            return Unit.VALUE;
        }, NOOP);
    }


    @Test
    public void testIterablesNoopDoesNotCloseAnything() throws Exception {

        CloseableIterable<String> result = databaseQuery.runInNewTransaction(connection -> iterateEmployeesFirstNames(connection, ClosePromise.NOOP()), NOOP);

        int count = consumerAndCount(result);
        assertThat(count, equalTo(10));
    }


    @Test
    public void testCloseableIterableHasNextDoesNotAffectResults() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            final CloseableIterator<Tuple> iterator = streamyResult.iterator();
            iterator.hasNext();
            iterator.hasNext();

            int remainingCount = 0;
            while (iterator.hasNext()) {
                iterator.next();
                remainingCount++;
            }

            assertThat(remainingCount, equalTo(TOTAL_NUMBER_EMPLOYEES_IN_DATASET));

            return Unit.VALUE;
        }, NOOP);

    }

    @Test
    public void testCloseableIterableNextCanBeCalledWithoutHasNext() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            final CloseableIterator<Tuple> iterator = streamyResult.iterator();
            for (int i = 0; i < TOTAL_NUMBER_EMPLOYEES_IN_DATASET; i++) {
                assertThat(iterator.next(), notNullValue());
            }

            return Unit.VALUE;
        }, NOOP);
    }

    @Test(expected = NoSuchElementException.class)
    public void testCloseableIterableNextThrowsExceptionIfNoMoreElements() throws Exception {
        databaseQuery.runInNewTransaction(dbConn -> {

            CloseableIterable<Tuple> streamyResult = streamEmployees(dbConn);

            final CloseableIterator<Tuple> iterator = streamyResult.iterator();
            for (int i = 0; i < TOTAL_NUMBER_EMPLOYEES_IN_DATASET + 1; i++) {
                assertThat(iterator.next(), notNullValue());
            }

            return Unit.VALUE;
        }, NOOP);
    }


    private <T> int consumerAndCount(final CloseableIterable<T> taken) {
        int count = 0;
        for (T ignored : taken) {
            count++;
        }
        return count;
    }

    private <T> int consume(final CloseableIterable<T> taken, int n) {
        int count = 0;
        for (T ignored : taken) {
            if (count >= n) {
                break;
            }
            count++;
        }
        return count;
    }

    /**
     * There are 10 results in this result set
     *
     * @return employees order by id
     */
    private CloseableIterable<Tuple> streamEmployees(DatabaseConnection connection) {
        return streamyQueryFactory.stream(connection, () -> connection.select(employee.firstname, employee.lastname).from(employee).orderBy(employee.id.asc()));
    }

    /**
     * There are 10 results in this result set
     *
     * @return employee first names order by id
     */
    private CloseableIterable<String> iterateEmployeesFirstNames(DatabaseConnection connection, ClosePromise closePromise) {
        return streamyQueryFactory.stream(connection, closePromise, () -> connection.select(employee.firstname).from(employee).orderBy(employee.id.asc()));
    }
}

