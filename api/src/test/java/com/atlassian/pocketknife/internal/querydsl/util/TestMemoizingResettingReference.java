package com.atlassian.pocketknife.internal.querydsl.util;

import com.google.common.base.Function;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestMemoizingResettingReference
{
    private CountingFunction countingFunction;

    @Before
    public void setUp() throws Exception
    {
        countingFunction = new CountingFunction();
    }

    /**
     * Mutable for tests
     */
    class CountingFunction implements Function<Integer, String>
    {
        int count = 0;

        boolean throwException = false;


        @Override
        public String apply(final Integer input)
        {
            count++;
            if (throwException)
            {
                throw new RuntimeException("As you wish!");
            }
            return String.valueOf(input);
        }

        public void setThrowException(final boolean throwException)
        {
            this.throwException = throwException;
        }

        int getCount()
        {
            return count;
        }
    }

    @Test
    public void test_only_called_once() throws Exception
    {
        // assemble
        MemoizingResettingReference<Integer, String> ref = new MemoizingResettingReference<>(countingFunction);

        // act
        String actual1 = ref.get(123);
        String actual2 = ref.get(456);

        // assert
        assertThat(actual1, equalTo("123"));
        assertThat(actual2, equalTo("123"));
        assertThat(countingFunction.getCount(), equalTo(1));
    }

    @Test
    public void test_reset() throws Exception
    {
        // assemble
        MemoizingResettingReference<Integer, String> ref = new MemoizingResettingReference<>(countingFunction);

        // act
        String actual1 = ref.get(123);
        String actual2 = ref.get(456);
        ref.reset();

        String actual3 = ref.get(789);


        // assert
        assertThat(actual1, equalTo("123"));
        assertThat(actual2, equalTo("123"));
        assertThat(actual3, equalTo("789"));
        assertThat(countingFunction.getCount(), equalTo(2));
    }

    @Test
    public void test_exception_throwing() throws Exception
    {
        // assemble
        MemoizingResettingReference<Integer, String> ref = new MemoizingResettingReference<>(countingFunction);

        // act
        countingFunction.setThrowException(true);
        try
        {
            ref.get(123);
        }
        catch (RuntimeException ignored)
        {
        }

        countingFunction.setThrowException(false);
        String actual = ref.get(456);

        // assert
        assertThat(actual, equalTo("456"));
        assertThat(countingFunction.getCount(), equalTo(2));
    }

    @Test (expected = NullPointerException.class)
    public void test_null_function_is_bad() throws Exception
    {
        // act
        new MemoizingResettingReference<>(null);
    }


    @Test (expected = NullPointerException.class)
    public void test_null_value_is_bad() throws Exception
    {
        // assemble
        MemoizingResettingReference<Object, Object> reference = new MemoizingResettingReference<>(new Function<Object, Object>()
        {
            @Override
            public Object apply(final Object input)
            {
                return null;
            }
        });
        // act
        reference.get(Unit.VALUE);
    }

    @Test (expected = MemoizingResettingReference.MemoizedValueNotPresentException.class)
    public void test_get_value_throws_because_no_one_called_get_before() throws Exception
    {
        // assemble
        MemoizingResettingReference<Integer, String> ref = new MemoizingResettingReference<>(countingFunction);

        // act
        ref.getMemoizedValue();
    }

    @Test
    public void test_get_value_works() throws Exception
    {
        // assemble
        MemoizingResettingReference<Integer, String> ref = new MemoizingResettingReference<>(countingFunction);

        ref.get(123);

        // act
        String actual = ref.getMemoizedValue();

        // assert
        assertThat(actual, equalTo("123"));
        assertThat(countingFunction.getCount(), equalTo(1));
    }

}