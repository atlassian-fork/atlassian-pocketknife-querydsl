package com.atlassian.pocketknife.internal.querydsl.mocks;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;
import com.atlassian.pocketknife.api.querydsl.configuration.ConfigurationEnrichment;
import com.atlassian.pocketknife.internal.querydsl.DatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.DatabaseConnectionConverterImpl;
import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearer;
import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearerImpl;
import com.atlassian.pocketknife.internal.querydsl.configuration.ConfigurationEnrichmentImpl;
import com.atlassian.pocketknife.internal.querydsl.dialect.DialectConfiguration;
import com.atlassian.pocketknife.internal.querydsl.schema.DatabaseSchemaCreation;
import com.atlassian.pocketknife.internal.querydsl.schema.DefaultSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.JdbcTableInspector;
import com.atlassian.pocketknife.internal.querydsl.schema.ProductSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.tables.TestConnections;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;

import java.util.Optional;

/**
 * Imagine this is a mini spring context containing the components that normally
 * would be wired into the world.  Then in tests you can call on them to do work,
 * especially in the "wired" real database tests
 */
public class TestingContext {

    private final DatabaseAccessor databaseAccessor;

    private final DatabaseConnectionConverter connectionConverter;

    private final TransactionalExecutorFactory transactionalExecutorFactory;

    private final ConfigurationEnrichment configurationEnrichment;

    private final DialectConfiguration dialectConfiguration;
    private final SchemaProvider schemaProvider;
    private final DebuggingDetailedListener debuggingDetailedListener;

    TestingContext(
            DialectConfiguration dialectConfiguration,
            SchemaProvider schemaProvider,
            ConfigurationEnrichment configurationEnrichment,
            MockTransactionExecutorFactory transactionalExecutorFactory,
            DebuggingDetailedListener debuggingDetailedListener) {
        DatabaseSchemaCreation databaseSchemaCreation = () -> {
        };
        this.debuggingDetailedListener = debuggingDetailedListener;
        this.transactionalExecutorFactory = transactionalExecutorFactory;
        this.configurationEnrichment = configurationEnrichment;
        this.schemaProvider = schemaProvider;
        this.dialectConfiguration = dialectConfiguration;
        this.connectionConverter = new DatabaseConnectionConverterImpl(dialectConfiguration);
        this.databaseAccessor = new DatabaseAccessorImpl(connectionConverter, this.transactionalExecutorFactory, databaseSchemaCreation);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static TestingContext forHsql() {
        return new Builder().forHsql().build();
    }

    public static TestingContext forH2() {
        return new Builder().forH2().build();
    }

    @Override
    public String toString() {
        return getLastSQL();
    }

    public String getLastSQL() {
        return debuggingDetailedListener.getLastSQL();
    }

    public SchemaProvider getSchemaProvider() {
        return schemaProvider;
    }

    public ConfigurationEnrichment getConfigurationEnrichment() {
        return configurationEnrichment;
    }

    public DatabaseConnectionConverter getConnectionConverter() {
        return connectionConverter;
    }

    public DatabaseAccessor getDatabaseAccessor() {
        return databaseAccessor;
    }

    public DialectConfiguration getDialectConfiguration() {
        return dialectConfiguration;
    }

    public TransactionalExecutorFactory getExecutorFactory() {
        return transactionalExecutorFactory;
    }


    public static class Builder {
        enum TargetDatabase {
            H2, HSQL
        }

        boolean withRealSchemaProvider = false;
        TargetDatabase targetDatabase = TargetDatabase.H2;

        Optional<String> schema = Optional.of("PUBLIC");

        public Builder forHsql() {
            targetDatabase = TargetDatabase.HSQL;
            withRealSchemaProvider = true;
            return this;
        }

        public Builder forH2() {
            targetDatabase = TargetDatabase.H2;
            withRealSchemaProvider = true;
            return this;
        }

        public Builder withSchema(String schemaName) {
            schema = Optional.ofNullable(schemaName);
            return this;
        }

        public Builder withNoSchema() {
            schema = Optional.empty();
            return this;
        }

        public TestingContext build() {

            MockTransactionExecutorFactory executorFactoryAccessor = new MockTransactionExecutorFactory();
            ConfigurationEnrichment configurationEnrichment = new ConfigurationEnrichmentImpl();
            SchemaProvider schemaProvider = new MockSchemaProvider(schema);
            if (withRealSchemaProvider) {
                ProductSchemaProvider productSchemaProvider = new ProductSchemaProvider(executorFactoryAccessor);
                JdbcTableInspector tableInspector = new JdbcTableInspector();

                PKQCacheClearer cacheClearer = new PKQCacheClearerImpl(null);
                schemaProvider = new DefaultSchemaProvider(productSchemaProvider, tableInspector, cacheClearer);
            }
            DebuggingDetailedListener detailedListener = new DebuggingDetailedListener();
            DialectConfiguration dialectConfiguration = new HsqlDialectConfiguration(schemaProvider, configurationEnrichment, detailedListener);

            // standard database
            try {
                if (targetDatabase == TargetDatabase.HSQL) {
                    TestConnections.initHSQL();
                } else {
                    TestConnections.initH2();
                }

            } catch (Exception e) {
                throw new RuntimeException("Yikes - This tests is in trouble", e);
            }

            return new TestingContext(dialectConfiguration, schemaProvider, configurationEnrichment, executorFactoryAccessor, detailedListener);
        }

    }
}
