package com.atlassian.pocketknife.api.querydsl.tuple;

import com.atlassian.pocketknife.api.querydsl.stream.ClosePromise;
import com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable;
import com.atlassian.pocketknife.internal.querydsl.stream.CloseableIterableImpl;
import com.atlassian.pocketknife.internal.querydsl.util.fp.Fp;
import com.google.common.base.Preconditions;
import com.mysema.commons.lang.CloseableIterator;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.NumberExpression;

import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * A class to handle processing Query Tuples of data
 */
public class Tuples
{
    /**
     * Sometimes you want a tuple to be returned even if you have only 1 column in the projection
     * so this helper makes that a smidge easier.
     *
     * @param expression the single expression
     * @return a single value array containing expression ready for {@link com.querydsl.sql.SQLQuery#select(Expression[])}
     */
    public static <T> Expression<T>[] tupleOf(Expression<T> expression) {
        //noinspection unchecked
        return new Expression[]{expression};
    }


    /**
     * Use this method to map a result of QueryDSL tuples into domain objects.
     * <p>
     * This will return an Iterable that closes itself after the last element has been retrieved.  This allows you to
     * use for each syntax to iterate database results without bring them all into memory at the one time
     * </p>
     *
     * @param closeableIterator the closeable iterator that was returned from a QueryDSL
     * @param f a function to map the QueryDSL tuples into actual domain objects
     * @param <T> the desired domain class
     * @return an Iterable that closes itself when the last record is retrieved.
     */
    public static <T> CloseableIterable<T> map(final CloseableIterator<Tuple> closeableIterator, final Function<Tuple, T> f)
    {
        return new CloseableIterableImpl<>(closeableIterator, f, ClosePromise.NOOP());
    }

    /**
     * Use this method to map a result of QueryDSL tuples into domain objects.
     * <p>
     * This will return an Iterable that closes itself after the last element has been retrieved.  This allows you to
     * use for each syntax to iterate database results without bring them all into memory at the one time
     * </p>
     * @param closeableIterator the closeable iterator that was returned from a QueryDSL
     * @param f a function to map the QueryDSL tuples into actual domain objects
     * @param closePromise this is the code you want to run when the underlying objects are closed
     * @param <T> the desired domain class
     * @return an Iterable that closes itself when the last record is retrieved.
     */
    public static <T> CloseableIterable<T> map(final CloseableIterator<Tuple> closeableIterator, final Function<Tuple, T> f, final ClosePromise closePromise)
    {
        return new CloseableIterableImpl<>(closeableIterator, f, closePromise);
    }

    /**
     * Use this method to limit a result of QueryDSL tuples by a certain amount and map it as the same time.
     * <p>
     * This will return an Iterable that closes itself after n elements is retrieved or the last element has been retrieved.  This allows you to
     * use for each syntax to iterate database results without bring them all into memory at the one time
     * </p>
     * @param closeableIterator the closeable iterator that was returned from a QueryDSL
     * @param f a function to map the QueryDSL tuples into actual domain objects
     * @param closePromise this is the code you want to run when the underlying objects are closed
     * @param n the number of elements to limit the iteration to
     * @param <T> the desired domain class
     * @return an Iterable that closes itself when the limit is reached or last record is retrieved.
     */
    public static <T> CloseableIterable<T> take(final CloseableIterator<Tuple> closeableIterator, final Function<Tuple, T> f, final ClosePromise closePromise, int n)
    {
        Preconditions.checkArgument(n >= 0, "take(n) must be >= 0");

        Predicate<Tuple> nTaken = CloseableIterableImpl.nTakenPredicate(n);
        return takeWhile(closeableIterator,f,closePromise,nTaken);
    }

    /**
     * Use this method to limit a result of QueryDSL tuples by a certain amount and map it as the same time.
     * <p>
     * This will return an Iterable that closes itself after the predicate no longer returns true or the last element has been retrieved.  This allows you to
     * use for each syntax to iterate database results without bring them all into memory at the one time
     * </p>
     * @param closeableIterator the closeable iterator that was returned from a QueryDSL
     * @param f a function to map the QueryDSL tuples into actual domain objects
     * @param closePromise this is the code you want to run when the underlying objects are closed
     * @param takeWhilePredicate the predicate to test against each tuple
     * @param <T> the desired domain class
     * @return an Iterable that closes itself when the limit is reached or last record is retrieved.
     */
    public static <T> CloseableIterable<T> takeWhile(final CloseableIterator<Tuple> closeableIterator, final Function<Tuple, T> f, final ClosePromise closePromise, Predicate<Tuple> takeWhilePredicate)
    {
        return new CloseableIterableImpl<>(closeableIterator, f, closePromise, Fp.alwaysTrue(), takeWhilePredicate);
    }

    /**
     * Use this method to limit a result of QueryDSL tuples by filtering the results and mapping it as the same time.
     * <p>
     * This will return an Iterable that will apply the filterPredicate to results and skip rows that do not match.
     * </p>
     * It then closes itself after the last element has been retrieved.  This allows you to
     * use for each syntax to iterate database results without bring them all into memory at the one time
     *
     * @param closeableIterator the closeable iterator that was returned from a QueryDSL
     * @param f a function to map the QueryDSL tuples into actual domain objects
     * @param closePromise this is the code you want to run when the underlying objects are closed
     * @param filterPredicate the predicate to filter rows in with
     * @param <T> the desired domain class
     * @return an Iterable that closes itself when the last record is retrieved.
     */
    public static <T> CloseableIterable<T> filter(final CloseableIterator<Tuple> closeableIterator, final Function<Tuple, T> f, final ClosePromise closePromise, Predicate<Tuple> filterPredicate)
    {
        return new CloseableIterableImpl<>(closeableIterator, f, closePromise, filterPredicate, Fp.alwaysTrue());
    }


    /**
     * This for each construct is a call back for every row returned.  When the last element is retrieved the database
     * iterator is closed.
     *
     * @param closeableIterator the closeable iterator that was returned from a QueryDSL
     * @param effect a Fugue call back effect
     */
    public static void foreach(final CloseableIterator<Tuple> closeableIterator, final Consumer<Tuple> effect)
    {
        foreach(closeableIterator, effect, ClosePromise.NOOP());
    }

    /**
     * This for each construct is a call back for every row returned.  When the last element is retrieved the database
     * iterator is closed.
     *
     * @param closeableIterator the closeable iterator that was returned from a QueryDSL
     * @param effect a Fugue call back effect
     * @param closePromise an effect to run when the underlying code is finished
     */
    public static void foreach(final CloseableIterator<Tuple> closeableIterator, final Consumer<Tuple> effect, final ClosePromise closePromise)
    {
        try
        {
            while (closeableIterator.hasNext())
            {
                Tuple t = closeableIterator.next();
                effect.accept(t);
            }
        }
        finally
        {
            closeQuietly(closeableIterator);
            closePromise.close();
        }
    }

    private static final TupleMapper tupleMapple = new TupleMapper();

    /**
     * A function to read from a Tuple containing the expression into its underlying value.
     *
     * @param expr the expression to examine and obtain from a tuple
     * @param <T> the underlying type of the expression
     * @return the value form the tuple as the type T
     */
    public static <T> Function<Tuple, T> column(final Expression<T> expr)
    {
        return tupleMapple.column(expr);
    }

    /**
     * A function to read from a Tuple containing the number expression into a BigDecimal
     *
     * @param expr the expression to examine and obtain from a tuple
     * @param <T> the underlying type of the number expression
     * @return the value form the tuple
     */
    public static <T extends Number & Comparable<?>> Function<Tuple, BigDecimal> toBigDecimal(final NumberExpression<T> expr)
    {
        return tupleMapple.toBigDecimal(expr);
    }

    /**
     * A function to read from a Tuple containing the number expression into a Long
     *
     * @param expr the expression to examine and obtain from a tuple
     * @param <T> the underlying type of the number expression
     * @return the value form the tuple
     */
    public static <T extends Number & Comparable<?>> Function<Tuple, Long> toLong(final NumberExpression<T> expr)
    {
        return tupleMapple.toLong(expr);
    }

    /**
     * A function to read from a Tuple containing the number expression into a Float
     *
     * @param expr the expression to examine and obtain from a tuple
     * @param <T> the underlying type of the number expression
     * @return the value form the tuple
     */
    public static <T extends Number & Comparable<?>> Function<Tuple, Float> toFloat(final NumberExpression<T> expr)
    {
        return tupleMapple.toFloat(expr);
    }

    /**
     * A function to read from a Tuple containing the number expression into a Integer
     *
     * @param expr the expression to examine and obtain from a tuple
     * @param <T> the underlying type of the number expression
     * @return the value form the tuple
     */
    public static <T extends Number & Comparable<?>> Function<Tuple, Integer> toInt(final NumberExpression<T> expr)
    {
        return tupleMapple.toInt(expr);
    }

    /**
     * A function to read from a Tuple containing the number expression into a Double
     *
     * @param expr the expression to examine and obtain from a tuple
     * @param <T> the underlying type of the number expression
     * @return the value form the tuple
     */
    public static <T extends Number & Comparable<?>> Function<Tuple, Double> toDouble(final NumberExpression<T> expr)
    {
        return tupleMapple.toDouble(expr);
    }

    private static void closeQuietly(final CloseableIterator<Tuple> closeableIterator)
    {
        try
        {
            closeableIterator.close();
        }
        catch (Exception ignored)
        {
        }
    }


}

