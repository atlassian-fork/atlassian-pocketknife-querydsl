package com.atlassian.pocketknife.api.querydsl.stream;

import com.atlassian.annotations.PublicApi;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.querydsl.sql.SQLQuery;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * StreamingQueryFactory gives you "streaming" access to the database.  You can read a large set of results and process them
 * without loading them all into memory.
 * <p>
 * Use this class wisely.
 * </p>
 * Database access in web applications is a precious resource.  In almost all cases you should obtain a connection, read a limited (re : paged) set of results
 * into memory and then return the connection as fast as possible.  Do not perform long processing of the results while you hold the database
 * connection open otherwise you will starve the other web request threads of access to the database.  Never obtain 2 or more connections from the
 * connection pool at the one time otherwise you may cause deadlocks as web request threads compete to get exclusive access to the database connections.
 * <p>
 * You should only use database streaming when you have a specific use case (say data import or data migration via an upgrade task) typically in a
 * background (non web request) thread.
 * <p>
 * The first rule of database streaming club, is don't use database streaming.  The second rule is, don't even think about database streaming.  And once
 * you have considered both rules closely, then, and only then, can you become a member of the database streaming club.
 * </p>
 */
@SuppressWarnings("UnusedDeclaration")
@PublicApi
@ParametersAreNonnullByDefault
public interface StreamingQueryFactory {
    /**
     * Allows you to obtain a streaming {@link CloseableIterable} of results using the provided connection.
     * <p>
     * <em>CONNECTION LIFECYCLE NOTES :</em> The connection will NOT be touched at all via this method.  It is up to the callee to handle the connection lifecycle.  The lifecycle of the
     * underlying {@link java.sql.ResultSet} will however be closed when the {@link CloseableIterable} is consumed.
     *
     * @param connection    the connection to use
     * @param querySupplier a supplier of the a {@link com.querydsl.sql.SQLQuery}
     * @return a streaming iterable of database results
     */
    <Q> CloseableIterable<Q> stream(DatabaseConnection connection, Supplier<SQLQuery<Q>> querySupplier);

    /**
     * Allows you to obtain a streaming {@link CloseableIterable} of results using the provided connection.
     * <p>
     * <em>CONNECTION LIFECYCLE NOTES :</em> The connection will NOT be touched at all via this method.  It is up to the callee to handle the connection lifecycle.  The lifecycle of the
     * underlying {@link java.sql.ResultSet} will however be closed when the {@link CloseableIterable} is consumed.  The {@link com.atlassian.pocketknife.api.querydsl.stream.ClosePromise}
     * will be executed when the  {@link CloseableIterable} is closed.
     *
     * @param connection    the connection to use
     * @param closePromise  a close promise to execute on closure of the  {@link CloseableIterable}
     * @param querySupplier a supplier of the a {@link com.querydsl.sql.SQLQuery}
     * @return a streaming iterable of database results
     */
    <Q> CloseableIterable<Q> stream(DatabaseConnection connection, ClosePromise closePromise, Supplier<SQLQuery<Q>> querySupplier);

    /**
     * Run the supplied closure with a streamy query then map over the result and return it, this function is not
     * lazy and will execute the method immediately and close the created  {@link CloseableIterable} which means that it will
     * pull all the transformed results into memory.
     *
     * @param closure The closure that will be executed
     * @param <T>     The type of List that will be returned
     * @return The result of running the query specified by
     * {@link com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory.StreamyFoldClosure#getQuery()}
     * then running the map from
     * {@link com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory.StreamyMapClosure#getMapFunction()}
     */
    <Q, T> List<T> streamyMap(DatabaseConnection connection, StreamyMapClosure<Q, T> closure);

    /**
     * Run the supplied closure with a streamy query then fold over the result and return it.
     *
     * @param initial The initial value to pass to the closure
     * @param closure The closure that will be executed
     * @param <T>     The type that is returned by the fold function
     * @return The result of running the query specified by {@link com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory.StreamyFoldClosure#getQuery()}
     * then running the fold from {@link com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory.StreamyFoldClosure#getFoldFunction()}
     */
    <Q, T> T streamyFold(DatabaseConnection connection, T initial, StreamyFoldClosure<Q, T> closure);


    /**
     * When running a {@link CloseableIterable} style query you will often end up
     * needing a closure as the mapping code needs to be shared between the query block and the processing function.
     * This interface formalises this closure so that the pattern can be easily applied also see
     * {@link #streamyFold(com.atlassian.pocketknife.api.querydsl.DatabaseConnection, Object, com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory.StreamyFoldClosure)}
     */
    static interface StreamyFoldClosure<Q, T> {
        /**
         * Returns the query for this closure, typically this will be passed into
         * {@link #stream(com.atlassian.pocketknife.api.querydsl.DatabaseConnection, java.util.function.Supplier)}
         *
         * @return A function that can be passed into stream to create a {@link com.querydsl.sql.SQLQuery}
         */
        Function<DatabaseConnection, SQLQuery<Q>> getQuery();

        /**
         * Returns the function that will be folded through the query.
         *
         * @return The function that will be passed to
         * {@link CloseableIterable#foldLeft(Object, java.util.function.BiFunction)}
         */
        BiFunction<T, Q, T> getFoldFunction();
    }

    /**
     * When running a {@link CloseableIterable} style query you will often end up
     * needing a closure as the mapping code needs to be shared between the query block and the processing function.
     * This interface formalises this closure for map so that the pattern can be easily applied also see
     * {@link StreamingQueryFactory#streamyMap(com.atlassian.pocketknife.api.querydsl.DatabaseConnection, com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory.StreamyMapClosure)}
     */
    static interface StreamyMapClosure<Q, T> {
        /**
         * Returns the query that will be run, this result of this function is typically used to create the
         *
         * @return A function that can be passed into stream to create a {@link com.querydsl.sql.SQLQuery}
         * via a call to {@link #stream(com.atlassian.pocketknife.api.querydsl.DatabaseConnection, java.util.function.Supplier)}
         */
        Function<DatabaseConnection, SQLQuery<Q>> getQuery();

        /**
         * The function that will be passed to
         * {@link CloseableIterable#map(java.util.function.Function)}
         *
         * @return a function that maps Tuples to T domain objects
         */
        Function<Q, T> getMapFunction();
    }
}
