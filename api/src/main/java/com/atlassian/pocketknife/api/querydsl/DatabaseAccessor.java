package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.pocketknife.api.querydsl.util.OnRollback;

import java.util.function.Function;

import static com.atlassian.pocketknife.api.querydsl.util.OnRollback.NOOP;

/**
 * This gives you the ability to access the database
 *
 * @since v2.0.0
 */
public interface DatabaseAccessor {

    /**
     * Gives you controlled access to a {@link com.atlassian.pocketknife.api.querydsl.DatabaseConnection} that allows
     * you to execute SQL statements as defined in the callback with managed transaction semantics.
     * <p>
     * This method will ALWAYS run the callback in a NEW transaction.  This is different to {@link #runInTransaction(Function)}
     * <p>
     * For that reason, the commit and rollback will be completely managed for you. If the callback returns
     * successfully, then this indicates that the underlying transaction is allowed to be committed at the appropriate
     * time. If you want to rollback the transaction then throw a RuntimeException. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you, then
     * specific {@link DatabaseConnection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link DatabaseConnection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link DatabaseConnection#commit()} commit will occur in the broader transaction context if no errors are encountered </li>
     * <li>{@link DatabaseConnection#rollback()} rollback will occur if a RuntimeException is thrown from the callback function </li>
     * <li>{@link DatabaseConnection#close()} the connection will be returned to the pool for you at the appropriate time</li>
     * </ul>
     * Example Usage:
     * <pre>
     *     databaseAccessor.runInNewTransaction(
     *         connection -&gt; {
     *             // throws RuntimeException to indicate rollback required
     *             doSomeUpdates(connection);
     *         }
     *     );
     * </pre>
     *
     * @param callback the callback function that can run one or more queries with the managed connection.
     * @param <T>      the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 4.1.0
     * @deprecated Use {@link #runInNewTransaction(Function, OnRollback)} instead. Since v4.4.0.
     */
    @Deprecated
    default <T> T runInNewTransaction(Function<DatabaseConnection, T> callback) {
        return runInNewTransaction(callback, NOOP);
    }

    /**
     * Gives you controlled access to a {@link com.atlassian.pocketknife.api.querydsl.DatabaseConnection} that allows
     * you to execute SQL statements as defined in the callback with managed transaction semantics.
     * <p>
     * This method will ALWAYS run the callback in a NEW transaction.  This is different to {@link #runInTransaction(Function, OnRollback)}
     * <p>
     * For that reason, the commit and rollback will be completely managed for you. If the callback returns
     * successfully, then this indicates that the underlying transaction is allowed to be committed at the appropriate
     * time. If you want to rollback the transaction then throw a RuntimeException. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you, then
     * specific {@link DatabaseConnection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link DatabaseConnection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link DatabaseConnection#commit()} commit will occur in the broader transaction context if no errors are encountered </li>
     * <li>{@link DatabaseConnection#rollback()} rollback will occur if a RuntimeException is thrown from the callback function </li>
     * <li>{@link DatabaseConnection#close()} the connection will be returned to the pool for you at the appropriate time</li>
     * </ul>
     * Example Usage:
     * <pre>
     *     databaseAccessor.runInNewTransaction(
     *         connection -&gt; {
     *             // throws RuntimeException to indicate rollback required
     *             doSomeUpdates(connection);
     *         }
     *     );
     * </pre>
     *
     * @param callback   the callback function that can run one or more queries with the managed connection.
     * @param onRollback A callback that will be executed in the event that transaction is rolled back
     * @param <T>        the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 4.4.0
     */
    <T> T runInNewTransaction(Function<DatabaseConnection, T> callback, OnRollback onRollback);

    /**
     * Gives you controlled access to a {@link com.atlassian.pocketknife.api.querydsl.DatabaseConnection} that allows
     * you to execute SQL statements as defined in the callback with managed transaction semantics.
     * <p>
     * This method will attempt to run the callback within an existing transaction if one is running within this thread.
     * For that reason, the commit and rollback will be completely managed for you. If the callback returns
     * successfully, then this indicates that the underlying transaction is allowed to be committed at the appropriate
     * time. If you want to rollback the transaction then throw a RuntimeException. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * <p>
     * This method is different to {@link #runInNewTransaction(Function)}
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you, then
     * specific {@link DatabaseConnection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link DatabaseConnection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link DatabaseConnection#commit()} commit will occur in the broader transaction context if no errors are encountered </li>
     * <li>{@link DatabaseConnection#rollback()} rollback will occur if a RuntimeException is thrown from the callback function </li>
     * <li>{@link DatabaseConnection#close()} the connection will be returned to the pool for you at the appropriate time</li>
     * </ul>
     * Example Usage:
     * <pre>
     *     databaseAccessor.runInTransaction(
     *         connection -&gt; {
     *             // throws RuntimeException to indicate rollback required
     *             doSomeUpdates(connection);
     *         }
     *     );
     * </pre>
     *
     * @param callback the callback function that can run one or more queries with the managed connection.
     * @param <T>      the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 2.0
     * @deprecated Use {@link #runInTransaction(Function, OnRollback)} instead. Since v4.4.0.
     */
    @Deprecated
    default <T> T runInTransaction(Function<DatabaseConnection, T> callback) {
        return runInTransaction(callback, NOOP);
    }

    /**
     * Gives you controlled access to a {@link com.atlassian.pocketknife.api.querydsl.DatabaseConnection} that allows
     * you to execute SQL statements as defined in the callback with managed transaction semantics.
     * <p>
     * This method will attempt to run the callback within an existing transaction if one is running within this thread.
     * For that reason, the commit and rollback will be completely managed for you. If the callback returns
     * successfully, then this indicates that the underlying transaction is allowed to be committed at the appropriate
     * time. If you want to rollback the transaction then throw a RuntimeException. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * <p>
     * This method is different to {@link #runInNewTransaction(Function, OnRollback)}
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you, then
     * specific {@link DatabaseConnection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link DatabaseConnection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link DatabaseConnection#commit()} commit will occur in the broader transaction context if no errors are encountered </li>
     * <li>{@link DatabaseConnection#rollback()} rollback will occur if a RuntimeException is thrown from the callback function </li>
     * <li>{@link DatabaseConnection#close()} the connection will be returned to the pool for you at the appropriate time</li>
     * </ul>
     * Example Usage:
     * <pre>
     *     databaseAccessor.runInTransaction(
     *         connection -&gt; {
     *             // throws RuntimeException to indicate rollback required
     *             doSomeUpdates(connection);
     *         }
     *     );
     * </pre>
     *
     * @param callback   the callback function that can run one or more queries with the managed connection.
     * @param onRollback A callback that will be executed in the event that transaction is rolled back
     * @param <T>        the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 4.4.0
     */
    <T> T runInTransaction(Function<DatabaseConnection, T> callback, OnRollback onRollback);

    /**
     * A synonym for the more semantically named {@link #runInTransaction(Function)} method
     *
     * @param callback the callback function that can run one or more queries with the managed connection.
     * @param <T>      the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 2.0. Until 4.3 this was a synonym for {@link #runInNewTransaction(Function)} method, however was
     * changed to make nested executions safer by default.
     * @deprecated Use {@link #run(Function, OnRollback)} instead. Since v4.4.0.
     */
    @Deprecated
    default <T> T run(Function<DatabaseConnection, T> callback) {
        return runInTransaction(callback);
    }

    /**
     * A synonym for the more semantically named {@link #runInTransaction(Function, OnRollback)} method
     *
     * @param callback   the callback function that can run one or more queries with the managed connection.
     * @param onRollback A callback that will be executed in the event that transaction is rolled back
     * @param <T>        the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 4.4.0
     */
    default <T> T run(Function<DatabaseConnection, T> callback, OnRollback onRollback) {
        return runInTransaction(callback, onRollback);
    }
}
