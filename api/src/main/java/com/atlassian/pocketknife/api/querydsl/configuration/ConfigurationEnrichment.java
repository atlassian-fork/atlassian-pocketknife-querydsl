package com.atlassian.pocketknife.api.querydsl.configuration;

import com.atlassian.annotations.PublicApi;
import com.atlassian.pocketknife.spi.querydsl.configuration.ConfigurationEnricher;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * This interface allows you to register "enrichers" of the underlying QueryDSL {@link com.querydsl.sql.Configuration} which
 * can be enriched with specific data types such as @{@link com.querydsl.sql.types.EnumByNameType}
 *
 * @since v3.1.0
 */
@PublicApi
@ParametersAreNonnullByDefault
public interface ConfigurationEnrichment {

    /**
     * @return the enricher in play
     */
    ConfigurationEnricher getEnricher();

    /**
     * Allows you to register an enricher into the QueryDSL environment, which must be done
     * before the connection is handed out to the PKQDSL consuming code.  During Spring wiring is a good
     * time to do this registration say.
     *
     * @param enricher the enricher to use
     */
    void setEnricher(ConfigurationEnricher enricher);
}
