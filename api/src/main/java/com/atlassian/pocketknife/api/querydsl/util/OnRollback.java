package com.atlassian.pocketknife.api.querydsl.util;

/**
 * Provide a callback mechanism for callers to know if the transaction was rolled back, and therefore considered
 * not successful, so any tidy up actions can be performed.
 *
 * @since v5.0.0
 */
@FunctionalInterface
public interface OnRollback {

    OnRollback NOOP = new NoopOnRollback();

    void execute();

    final class NoopOnRollback implements OnRollback {
        @Override
        public void execute() {
            // does nothing
        }
    }

}
