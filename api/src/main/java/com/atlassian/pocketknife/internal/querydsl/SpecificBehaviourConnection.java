package com.atlassian.pocketknife.internal.querydsl;

import javax.annotation.Nonnull;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * An implementation of Connection that allows certain methods but delegates to the asserted behaviour otherwise
 *
 * @since v0.x
 */
class SpecificBehaviourConnection implements Connection {
    private final Connection salConnection;

    SpecificBehaviourConnection(@Nonnull Connection connection) {
        this.salConnection = connection;
    }

    private Connection underlying(@Nonnull Connection connection) {
        String implementationClassName = connection.getClass().getCanonicalName();
        if ("com.atlassian.sal.core.rdbms.WrappedConnection".equals(implementationClassName)) {
            try {
                return connection.getMetaData().getConnection();
            } catch (SQLException e) {
                throw new RuntimeException("Unable to call connection.getMetaData()", e);
            }
        }
        return connection;
    }

    //----------------------------------------
    //
    // the methods we want specific allowable behavior on

    @Override
    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
        underlying(salConnection).releaseSavepoint(savepoint);
    }

    @Override
    public void rollback(Savepoint savepoint) throws SQLException {
        underlying(salConnection).rollback(savepoint);
    }

    @Override
    public Savepoint setSavepoint() throws SQLException {
        return underlying(salConnection).setSavepoint();
    }

    @Override
    public Savepoint setSavepoint(String name) throws SQLException {
        return underlying(salConnection).setSavepoint(name);
    }

    //----------------------------------------
    //
    // - the rest we leave as is

    @Override
    public void abort(Executor executor) throws SQLException {
        salConnection.abort(executor);
    }

    @Override
    public void clearWarnings() throws SQLException {
        salConnection.clearWarnings();
    }

    @Override
    public void close() throws SQLException {
        salConnection.close();
    }

    @Override
    public void commit() throws SQLException {
        salConnection.commit();
    }

    @Override
    public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
        return salConnection.createArrayOf(typeName, elements);
    }

    @Override
    public Blob createBlob() throws SQLException {
        return salConnection.createBlob();
    }

    @Override
    public Clob createClob() throws SQLException {
        return salConnection.createClob();
    }

    @Override
    public NClob createNClob() throws SQLException {
        return salConnection.createNClob();
    }

    @Override
    public SQLXML createSQLXML() throws SQLException {
        return salConnection.createSQLXML();
    }

    @Override
    public Statement createStatement() throws SQLException {
        return salConnection.createStatement();
    }

    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
        return salConnection.createStatement(resultSetType, resultSetConcurrency);
    }

    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        return salConnection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    @Override
    public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
        return salConnection.createStruct(typeName, attributes);
    }

    @Override
    public boolean getAutoCommit() throws SQLException {
        return salConnection.getAutoCommit();
    }

    @Override
    public String getCatalog() throws SQLException {
        return salConnection.getCatalog();
    }

    @Override
    public Properties getClientInfo() throws SQLException {
        return salConnection.getClientInfo();
    }

    @Override
    public String getClientInfo(String name) throws SQLException {
        return salConnection.getClientInfo(name);
    }

    @Override
    public int getHoldability() throws SQLException {
        return salConnection.getHoldability();
    }

    @Override
    public DatabaseMetaData getMetaData() throws SQLException {
        return salConnection.getMetaData();
    }

    @Override
    public int getNetworkTimeout() throws SQLException {
        return salConnection.getNetworkTimeout();
    }

    @Override
    public String getSchema() throws SQLException {
        return salConnection.getSchema();
    }

    @Override
    public int getTransactionIsolation() throws SQLException {
        return salConnection.getTransactionIsolation();
    }

    @Override
    public Map<String, Class<?>> getTypeMap() throws SQLException {
        return salConnection.getTypeMap();
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        return salConnection.getWarnings();
    }

    @Override
    public boolean isClosed() throws SQLException {
        return salConnection.isClosed();
    }

    @Override
    public boolean isReadOnly() throws SQLException {
        return salConnection.isReadOnly();
    }

    @Override
    public boolean isValid(int timeout) throws SQLException {
        return salConnection.isValid(timeout);
    }

    @Override
    public String nativeSQL(String sql) throws SQLException {
        return salConnection.nativeSQL(sql);
    }

    @Override
    public CallableStatement prepareCall(String sql) throws SQLException {
        return salConnection.prepareCall(sql);
    }

    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
        return salConnection.prepareCall(sql, resultSetType, resultSetConcurrency);
    }

    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        return salConnection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    @Override
    public PreparedStatement prepareStatement(String sql) throws SQLException {
        return salConnection.prepareStatement(sql);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
        return salConnection.prepareStatement(sql, autoGeneratedKeys);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
        return salConnection.prepareStatement(sql, columnIndexes);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
        return salConnection.prepareStatement(sql, columnNames);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
        return salConnection.prepareStatement(sql, resultSetType, resultSetConcurrency);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        return salConnection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }


    @Override
    public void rollback() throws SQLException {
        salConnection.rollback();
    }


    @Override
    public void setAutoCommit(boolean autoCommit) throws SQLException {
        salConnection.setAutoCommit(autoCommit);
    }

    @Override
    public void setCatalog(String catalog) throws SQLException {
        salConnection.setCatalog(catalog);
    }

    @Override
    public void setClientInfo(String name, String value) throws SQLClientInfoException {
        salConnection.setClientInfo(name, value);
    }

    @Override
    public void setClientInfo(Properties properties) throws SQLClientInfoException {
        salConnection.setClientInfo(properties);
    }

    @Override
    public void setHoldability(int holdability) throws SQLException {
        salConnection.setHoldability(holdability);
    }

    @Override
    public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
        salConnection.setNetworkTimeout(executor, milliseconds);
    }

    @Override
    public void setReadOnly(boolean readOnly) throws SQLException {
        salConnection.setReadOnly(readOnly);
    }


    @Override
    public void setSchema(String schema) throws SQLException {
        salConnection.setSchema(schema);
    }

    @Override
    public void setTransactionIsolation(int level) throws SQLException {
        salConnection.setTransactionIsolation(level);
    }

    @Override
    public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
        salConnection.setTypeMap(map);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return salConnection.isWrapperFor(iface);
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return salConnection.unwrap(iface);
    }
}
