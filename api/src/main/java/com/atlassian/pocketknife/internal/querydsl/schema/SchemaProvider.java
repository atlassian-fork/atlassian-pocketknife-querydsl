package com.atlassian.pocketknife.internal.querydsl.schema;

import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Connection;
import java.util.Optional;

/**
 * A helper that can be used to provide database names (schema, table, column) at runtime.
 * <p>
 * Useful for retrieving names in environments where case changes between database engines,
 * or where names aren't known ahead of time.
 */
@ParametersAreNonnullByDefault
public interface SchemaProvider
{
    /**
     * @return the schema name from the underlying product environment
     */
    Optional<String> getProductSchema();

    /**
     * Retrieve the case-sensitive table name of the actual table that matches the given table name (if it exists).
     *
     * @param connection the connection in play
     * @param logicalTableName the logical table to lookup
     *
     * @return The actual (case-sensitive) table name for the given table, or <code>null</code> if none exists.
     */
    Optional<String> getTableName(Connection connection, String logicalTableName);

    /**
     * Retrieve the case-sensitive column name of the column in the given table (if it exists).
     *
     * @param connection the connection in play
     * @param logicalTableName the logical table to look for columns in
     * @param logicalColumnName the logical column to lookup
     *
     * @return The actual (case-sensitive) column name for the column in the given table,
     * or <code>null</code> if none is found.
     */
    Optional<String> getColumnName(Connection connection, String logicalTableName, String logicalColumnName);
}
