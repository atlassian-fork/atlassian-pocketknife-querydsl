package com.atlassian.pocketknife.internal.querydsl.schema;

import com.querydsl.core.types.Path;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.Configuration;
import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.SchemaAndTable;

import java.sql.Connection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * This can add overrides into a configuration so tables get their RDBMS name
 */
public class SchemaOverrider {

    private final SchemaProvider schemaProvider;

    public SchemaOverrider(SchemaProvider schemaProvider) {
        this.schemaProvider = schemaProvider;
    }

    /**
     * This will mutate the QueryDSL configuration and put in overrides if the actual
     * table and column names are different to how they have been declared in the
     * relational entity.
     *
     * @param connection      a connection to hit the actual underlying database with
     * @param configuration   the QDSL configuration to mutate with overrides
     * @param relationalPaths the tables to possible override
     *
     * @return the configuration for fluent style reasons
     */
    public Configuration registerOverrides(Connection connection, Configuration configuration, Set<RelationalPath<?>> relationalPaths) {
        for (RelationalPath<?> relationalPath : relationalPaths) {
            registerTableOverride(connection, configuration, relationalPath);
        }
        return configuration;
    }

    private void registerTableOverride(Connection connection, Configuration configuration, RelationalPath<?> relationalPath) {

        final SchemaAndTable currentSchemaAndTable = relationalPath.getSchemaAndTable();
        final String logicalTableName = relationalPath.getTableName();
        final String actualSchemaName = schemaProvider.getProductSchema().orElse(null);

        /*
         * Because inActiveObjects chose to "quote" and "uppercase" table and column names, the code ends up in a bind if it wants to also
         * use tables that don't use quoting such as core application tables.  And because the table case is database
         * specific (when not quoted) then static code cannot know what name to use at compile time.
         *
         * So we need methods to allows us to query the database (via a cached Supplier mechanism) to find the ACTUAL table name at
         * runtime and hence the quoting becomes irrelevant.
         *
         * But this has a cost since we cache in memory (bad) and hit the database (bad) so we have an optimisation here.
         *
         * if its an AO table we ALWAYS uppercase it and hence we don't have to hit the database nor cache anything and since
         * AO tables make up the bulk of all tables in PKQDSL land this is a good optimisation.
         *
         * But if its not an AO table then we are forced to lookup the table form the database and cache it for performance reasons.
         *
         */
        final boolean isAOTable = isAOTable(logicalTableName);
        final String actualTableName;
        if (isAOTable) {
            actualTableName = logicalTableName.toUpperCase();
        } else {
            // default to logical if its not known
            actualTableName = schemaProvider.getTableName(connection, logicalTableName).orElse(logicalTableName);
        }

        final SchemaAndTable replacementSchemaAndTable = new SchemaAndTable(actualSchemaName, actualTableName);

        // table overrides if needed
        if (!Objects.equals(replacementSchemaAndTable, currentSchemaAndTable)) {
            configuration.registerTableOverride(currentSchemaAndTable, replacementSchemaAndTable);
        }

        // column overrides if needed
        final List<Path<?>> columns = relationalPath.getColumns();
        for (Path<?> column : columns) {
            final String logicalColumnName = ColumnMetadata.getName(column);
            final String actualColumnName;
            if (isAOTable) {
                actualColumnName = logicalColumnName.toUpperCase();
            } else {
                // default to logical if its not known
                actualColumnName = schemaProvider.getColumnName(connection, logicalTableName, logicalColumnName).orElse(logicalColumnName);
            }

            if (!Objects.equals(logicalColumnName, actualColumnName)) {
                configuration.registerColumnOverride(logicalTableName, logicalColumnName, actualColumnName);
            }
        }
    }

    private boolean isAOTable(String logical) {
        // an AO table looks like "AO_23EF_TABLE_NAME"
        return logical.toUpperCase().startsWith("AO_");
    }
}
