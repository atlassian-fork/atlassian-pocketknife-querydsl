package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Connection;
import java.sql.SQLException;

import static com.google.common.base.Preconditions.checkState;

/**
 */
@Component
@ParametersAreNonnullByDefault
public class DatabaseConnectionConverterImpl implements DatabaseConnectionConverter {
    private final DialectProvider dialectProvider;

    @Autowired
    public DatabaseConnectionConverterImpl(final DialectProvider dialectProvider) {
        this.dialectProvider = dialectProvider;
    }

    @Override
    public DatabaseConnection convert(final Connection jdbcConnection) {
        return convertImpl(jdbcConnection, false);
    }

    @Override
    public DatabaseConnection convertExternallyManaged(Connection jdbcConnection) {
        return convertImpl(jdbcConnection, true);
    }

    private DatabaseConnection convertImpl(final Connection jdbcConnection, boolean managedConnection) {
        assertNotClosed(jdbcConnection);

        DialectProvider.Config dialectConfig = getDialectConfig(jdbcConnection);
        SpecificBehaviourConnection specificBehaviourConnection = new SpecificBehaviourConnection(jdbcConnection);
        return new DatabaseConnectionImpl(dialectConfig, specificBehaviourConnection, managedConnection);
    }

    private DialectProvider.Config getDialectConfig(final Connection jdbcConnection) {
        return dialectProvider.getDialectConfig(assertNotClosed(jdbcConnection));
    }

    private Connection assertNotClosed(final Connection jdbcConnection) {
        try {
            checkState(!jdbcConnection.isClosed());
        } catch (SQLException e) {
            throw new IllegalStateException("PKQDSL is unable to assert that the JDBC connection is not closed", e);
        }
        return jdbcConnection;
    }

}
