package com.atlassian.pocketknife.spi.querydsl;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EnumPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.core.types.dsl.TimePath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.PrimaryKey;
import com.querydsl.sql.RelationalPathBase;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Enhances {@link RelationalPathBase} with additional functionality designed to make writing and executing queries
 * using table entities that extend this class easier.
 * <p>
 * This extra functionality includes:
 * <ul>
 * <li>
 * Dynamic schema lookup: QueryDSL table classes that extend this class can be instantiated without specifying
 * a schema. This allows them to be declared as static variables and used at call sites and yet still have the
 * capability of knowing the underlying schema name which is not known until runtime.
 * </li>
 * <li>
 * Implicitly adding any columns defined by a call to the create methods to the metadata for table entity. This
 * allows calls to methods like {@link com.querydsl.sql.RelationalPathBase#getColumns()} to actually work
 * correctly.
 * </li>
 * <li>
 * Adds column builder patterns to allow very specific column paths to be constructed.  This allows for not null
 * specification as well as primary key
 * </li>
 * <li>
 * Adds convenience methods like {@link #getAllNonPrimaryKeyColumns()}
 * </li>
 * </ul>
 */
@SuppressWarnings("UnusedDeclaration")
public abstract class EnhancedRelationalPathBase<T> extends RelationalPathBase<T> {
    private static final Logger log = getLogger(EnhancedRelationalPathBase.class);

    private static final String DEFAULT_SCHEMA = "";

    /**
     * We need a java type for enums, and since they are stored in the database typically strings
     * then we use that.  if some one really wants to get funky then they will have to use
     * {@link #createEnumCol} to set specific types
     */
    private static final int ENUM_JAVA_TYPE = Types.VARCHAR;

    /**
     * Creates an EnhancedRelationalPath with the logical table name
     *
     * @param type             the class of type T
     * @param logicalTableName the logical name of the table this represents
     */
    public EnhancedRelationalPathBase(
            final Class<? extends T> type,
            final String logicalTableName) {
        super(type, PathMetadataFactory.forVariable(logicalTableName), DEFAULT_SCHEMA, logicalTableName);
    }

    /**
     * Creates an EnhancedRelationalPath with the logical table name
     *
     * @param type             the class of type T
     * @param logicalTableName the logical name of the table this represents
     * @param tableAlias       the alias to use in queries eg: 'select * from logicalTableName tableAlias where ....'
     */
    public EnhancedRelationalPathBase(
            final Class<? extends T> type,
            final String logicalTableName,
            final String tableAlias) {
        super(type, PathMetadataFactory.forVariable(tableAlias), DEFAULT_SCHEMA, logicalTableName);
    }


    //
    // the following override the base path construction methods so that metadata is added at the same time
    // as the path creation
    //

    /**
     * Creates a boolean column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    @Override
    protected BooleanPath createBoolean(final String columnName) {
        BooleanPath path = super.createBoolean(columnName);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.BOOLEAN));
        return path;
    }

    /**
     * Creates a date column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    @Override
    protected <A extends Comparable> DatePath<A> createDate(final String columnName, final Class<? super A> type) {
        DatePath<A> path = super.createDate(columnName, type);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.DATE));
        return path;
    }

    /**
     * Creates a datetime column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    @Override
    protected <A extends Comparable> DateTimePath<A> createDateTime(final String columnName, final Class<? super A> type) {
        DateTimePath<A> path = super.createDateTime(columnName, type);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.TIMESTAMP));
        return path;
    }

    /**
     * Creates a number column with sensible default metadata.
     *
     * <em>Note:</em> this method should only be called when the number type is not known at compile time. Typically,
     * this type is known, in which case the appropriate method for that type should be called instead
     * (e.g. {@link #createInteger(java.lang.String)}.
     *
     * @param columnName the name of the column
     * @param type       the type of the column
     *
     * @return the new path
     */
    @Override
    protected <A extends Number & Comparable<?>> NumberPath<A> createNumber(final String columnName, final Class<? super A> type) {
        NumberPath<A> path = super.createNumber(columnName, type);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(mapJavaNumberType(type)));
        return path;
    }

    /**
     * Creates an integer column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    protected NumberPath<Integer> createInteger(final String columnName) {
        NumberPath<Integer> path = super.createNumber(columnName, Integer.class);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.INTEGER));
        return path;
    }

    /**
     * Creates a long column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    protected NumberPath<Long> createLong(final String columnName) {
        NumberPath<Long> path = super.createNumber(columnName, Long.class);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.BIGINT));
        return path;
    }

    /**
     * Creates a double column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    protected NumberPath<Double> createDouble(final String columnName) {
        NumberPath<Double> path = super.createNumber(columnName, Double.class);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.DOUBLE));
        return path;
    }

    /**
     * Creates a big decimal column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    protected NumberPath<BigDecimal> createBigDecimal(final String columnName) {
        NumberPath<BigDecimal> path = super.createNumber(columnName, BigDecimal.class);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.DECIMAL));
        return path;
    }

    /**
     * Creates a float column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    protected NumberPath<Float> createFloat(final String columnName) {
        NumberPath<Float> path = super.createNumber(columnName, Float.class);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.DECIMAL));
        return path;
    }

    /**
     * Creates a string column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    @Override
    protected StringPath createString(final String columnName) {
        StringPath path = super.createString(columnName);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.VARCHAR));
        return path;
    }

    /**
     * Creates a time column with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    @Override
    protected <A extends Comparable> TimePath<A> createTime(final String columnName, final Class<? super A> type) {
        TimePath<A> path = super.createTime(columnName, type);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.TIME));
        return path;
    }

    /**
     * Creates a column based on an enumeration with sensible default metadata
     *
     * @param columnName the name of the column
     *
     * @return the new path
     */
    @Override
    protected <A extends Enum<A>> EnumPath<A> createEnum(String columnName, Class<A> type) {
        EnumPath<A> path = super.createEnum(columnName, type);
        addMetadata(path, ColumnMetadata.named(columnName).ofType(Types.VARCHAR));
        return path;
    }

    //
    // the following are the builder style column creation methods that allow full metadata creation
    //

    /**
     * Creates a boolean column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected ColumnWithMetadataBuilder<BooleanPath> createBooleanCol(final String columnName) {
        BooleanPath path = super.createBoolean(columnName);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.BOOLEAN));
    }

    /**
     * Creates a date column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected <A extends Comparable> ColumnWithMetadataBuilder<DatePath<A>> createDateCol(final String columnName, final Class<? super A> type) {
        DatePath<A> path = super.createDate(columnName, type);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.DATE));
    }

    /**
     * Creates a date time column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected <A extends Comparable> ColumnWithMetadataBuilder<DateTimePath<A>> createDateTimeCol(final String columnName, final Class<? super A> type) {
        DateTimePath<A> path = super.createDateTime(columnName, type);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.TIMESTAMP));
    }


    /**
     * Creates a Number column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected <A extends Number & Comparable<?>> ColumnWithMetadataBuilder<NumberPath<A>> createNumberCol(final String columnName, final Class<? super A> type) {
        // The method below does not compile with Java 6 due to a bug in the Java 6 JDK: http://bugs.java.com/view_bug.do?bug_id=6302954
        // This bug has been fixed in Java 7, so when we only support that, this method can be restored. For now, it has been
        // replaced with Number subclass specific versions.
        NumberPath<A> path = super.createNumber(columnName, type);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(mapJavaNumberType(type)));
    }

    /**
     * Creates an integer column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected ColumnWithMetadataBuilder<NumberPath<Integer>> createIntegerCol(final String columnName) {
        NumberPath<Integer> path = super.createNumber(columnName, Integer.class);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.INTEGER));
    }

    /**
     * Creates a long column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected ColumnWithMetadataBuilder<NumberPath<Long>> createLongCol(final String columnName) {
        NumberPath<Long> path = super.createNumber(columnName, Long.class);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.BIGINT));
    }

    /**
     * Creates a double column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected ColumnWithMetadataBuilder<NumberPath<Double>> createDoubleCol(final String columnName) {
        NumberPath<Double> path = super.createNumber(columnName, Double.class);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.DOUBLE));
    }

    /**
     * Creates a big decimal column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected ColumnWithMetadataBuilder<NumberPath<BigDecimal>> createBigDecimalCol(final String columnName) {
        NumberPath<BigDecimal> path = super.createNumber(columnName, BigDecimal.class);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.DECIMAL));
    }

    /**
     * Creates a float column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected ColumnWithMetadataBuilder<NumberPath<Float>> createFloatCol(final String columnName) {
        NumberPath<Float> path = super.createNumber(columnName, Float.class);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.DECIMAL));
    }

    /**
     * Creates a string column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected ColumnWithMetadataBuilder<StringPath> createStringCol(final String columnName) {
        StringPath path = super.createString(columnName);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.VARCHAR));
    }

    /**
     * Creates a time column with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected <A extends Comparable> ColumnWithMetadataBuilder<TimePath<A>> createTimeCol(final String columnName, final Class<? super A> type) {
        TimePath<A> path = super.createTime(columnName, type);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.TIME));
    }

    /**
     * Creates a column based on an enumeration with sensible default metadata that you can add to
     *
     * @param columnName the name of the column
     *
     * @return the builder of column metadata
     */
    protected <A extends Enum<A>> ColumnWithMetadataBuilder<EnumPath<A>> createEnumCol(final String columnName, final Class<A> type) {
        EnumPath<A> path = super.createEnum(columnName, type);
        return new ColumnWithMetadataBuilder<>(path, ColumnMetadata.named(columnName).ofType(Types.VARCHAR));
    }

    /**
     * @return an array of all the paths that are not primary keys.  Useful for inserts.
     *
     * @see #getColumns()
     */
    public Path<?>[] getAllNonPrimaryKeyColumns() {
        final PrimaryKey<T> primaryKey = getPrimaryKey();
        // primaryKey can be null as can its local columns
        final List<? extends Path<?>> pkColumns = (primaryKey != null && primaryKey.getLocalColumns() != null)
                ? primaryKey.getLocalColumns()
                : Collections.<Path<?>>emptyList();
        List<Path<?>> columns = newArrayList(getColumns().stream().filter(input -> {
            for (Path<?> pkColumn : pkColumns) {
                if (pkColumn.equals(input)) {
                    return false;
                }
            }
            return true;
        }).collect(Collectors.toList()));

        return newArrayList(columns).toArray(new Path[columns.size()]);
    }

    private int mapJavaNumberType(final Class<?> javaType) {
        if (javaType.equals(Integer.class) || javaType.equals(Integer.TYPE)) {
            return Types.INTEGER;
        } else if (javaType.equals(Long.class) || javaType.equals(Long.TYPE)) {
            return Types.BIGINT;
        } else if (javaType.equals(Double.class) || javaType.equals(Double.TYPE)) {
            return Types.DOUBLE;
        } else if (javaType.equals(Float.class) || javaType.equals(Float.TYPE)) {
            return Types.DECIMAL;
        } else {
            throw new UnsupportedOperationException("Unable to map number class " + javaType + " to JDBC type");
        }
    }

    /**
     * This allows a path to be build up in a builder style at the entity declaration point
     * <pre>
     *      StringPath sCol = createStringCol("COL_NAME").withIndex(1).ofType(Types.VARCHAR).notNull().asPrimaryKey().build();
     * </pre>
     *
     * @param <P> a {@link com.querydsl.core.types.Path} type
     *
     * @see com.querydsl.sql.ColumnMetadata
     */
    public class ColumnWithMetadataBuilder<P extends Path<?>> {
        private final P path;
        private ColumnMetadata metadata;
        private boolean asPK = false;

        public ColumnWithMetadataBuilder(final P path, final ColumnMetadata startingMetadata) {
            this.path = path;
            this.metadata = startingMetadata;
        }

        public ColumnWithMetadataBuilder<P> asPrimaryKey() {
            asPK = true;
            metadata = metadata.notNull(); // PK cant be null
            return this;
        }

        public ColumnWithMetadataBuilder<P> notNull() {
            metadata = metadata.notNull();
            return this;
        }

        public ColumnWithMetadataBuilder<P> ofType(int jdbcType) {
            metadata = metadata.ofType(jdbcType);
            return this;
        }

        public ColumnWithMetadataBuilder<P> withIndex(int index) {
            metadata = metadata.withIndex(index);
            return this;
        }

        public ColumnWithMetadataBuilder<P> withSize(int size) {
            metadata = metadata.withSize(size);
            return this;
        }

        public ColumnWithMetadataBuilder<P> withDigits(int decimalDigits) {
            metadata = metadata.withDigits(decimalDigits);
            return this;
        }

        /**
         * Builds the column path and metadata in one step
         *
         * @return the new path
         */
        public P build() {
            addMetadata(path, metadata);
            if (asPK) {
                PrimaryKey<T> currentPK = getPrimaryKey();
                if (currentPK != null) {
                    throw new IllegalStateException("You have already set a primary key.  I am not sure you know what you are doing");
                }
                createPrimaryKey(path);
            }
            return path;
        }
    }
}
