package com.atlassian.pocketknife.spi.querydsl.configuration;

import com.atlassian.annotations.PublicSpi;
import com.querydsl.sql.Configuration;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Called to allow enrichment of the QueryDSL {@link com.querydsl.sql.Configuration}
 *
 * @since v3.1.0
 */
@PublicSpi
@ParametersAreNonnullByDefault
public interface ConfigurationEnricher {

    /**
     * You will be called with the QueryDSL {@link Configuration} that will be used by PKDQSL
     * consuming code.
     *
     * @param configuration the configuration you can enrich
     */
    Configuration enrich(Configuration configuration);
}
