package com.atlassian.pocketknife.test.util.querydsl;

import org.junit.Test;

public class StandaloneDatabaseAccessorUtilsTest {

    @Test
    public void shouldNotThrowWhenDroppingNonExistentTable() {
        // Invoke
        StandaloneDatabaseAccessorUtils.dropTable("nosuchtable", new StandaloneDatabaseAccessor());
    }
}