package com.atlassian.pocketknife.test.util.querydsl;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.ExpectedException;

public class StandaloneDatabaseAccessorSystemPropertiesTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Test
    public void testCustomProperties() throws Exception {
        System.setProperty(StandaloneDatabaseAccessor.SYS_PROP_URL, "someUrl");
        System.setProperty(StandaloneDatabaseAccessor.SYS_PROP_USER, "someUser");
        System.setProperty(StandaloneDatabaseAccessor.SYS_PROP_PASSWORD, "somePassword");

        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("unable to connect");
        expectedException.expectMessage("someUrl");
        expectedException.expectMessage("someUser");
        expectedException.expectMessage("somePassword");

        new StandaloneDatabaseAccessor();
    }
}
