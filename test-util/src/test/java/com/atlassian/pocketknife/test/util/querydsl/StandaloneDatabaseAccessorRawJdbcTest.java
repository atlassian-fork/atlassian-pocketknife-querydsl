package com.atlassian.pocketknife.test.util.querydsl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class StandaloneDatabaseAccessorRawJdbcTest {

    private StandaloneDatabaseAccessor databaseAccessor;

    @Before
    public void setUp() throws Exception {
        databaseAccessor = new StandaloneDatabaseAccessor();

        dropTable();

        try (Connection connection = databaseAccessor.connection())
        {
            connection.prepareStatement("" +
                        "create table ao_012345_some_table ( " +
                        "    id integer not null, " +
                        "    val varchar(255) not null, " +
                        "    primary key (id) " +
                        ") "
            ).execute();
        }
    }

    @After
    public void tearDown() throws Exception {
        dropTable();
    }

    private void dropTable() {
        try (Connection connection = databaseAccessor.connection()) {
            connection.prepareStatement("" +
                    "drop table ao_012345_some_table"
            ).execute();
        } catch (SQLException e) {
            // squish as table probably doesn't actually exist
        }
    }

    @Test
    public void testInsertSelect() throws Exception {

        try (Connection connection = databaseAccessor.connection())
        {
            connection.prepareStatement("" +
                            "insert into ao_012345_some_table (id, val) " +
                            "values (1, 'val1') "
            ).execute();

            final String select = "" +
                    "select val " +
                    "from ao_012345_some_table " +
                    "where id = 1 ";

            final ResultSet rs = connection.prepareStatement(select).executeQuery();

            assertThat("no results returned for " + select, rs.next(), is(true));
            assertThat(rs.getString("VAL"), is("val1"));
        }
    }

    @Test
    public void testInsertDelete() throws Exception {
        try (Connection connection = databaseAccessor.connection())
        {
            connection.prepareStatement("" +
                            "insert into ao_012345_some_table (id, val) " +
                            "values (1, 'val1') "
            ).execute();

            connection.prepareStatement("" +
                    "delete from ao_012345_some_table " +
                    "where id = 1 ").execute();

            final String select = "" +
                    "select val " +
                    "from ao_012345_some_table " +
                    "where id = 1 ";

            final ResultSet rs = connection.prepareStatement(select).executeQuery();

            assertThat("results returned for " + select, rs.next(), is(false));
        }
    }
}
