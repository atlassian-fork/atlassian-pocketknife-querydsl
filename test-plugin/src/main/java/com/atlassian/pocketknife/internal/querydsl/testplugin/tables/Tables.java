package com.atlassian.pocketknife.internal.querydsl.testplugin.tables;

/**
 * Our QueryDSL tables
 */
public class Tables
{
    private static final String AO = "AO_C0458A_";  // <-- namespace

    public static final QCustomerTable CUSTOMER = new QCustomerTable(AO + "CUSTOMER");
    public static final QAddressTable ADDRESS = new QAddressTable(AO + "ADDRESS");
    public static final QIssueTable ISSUE = new QIssueTable("jiraissue");
}
