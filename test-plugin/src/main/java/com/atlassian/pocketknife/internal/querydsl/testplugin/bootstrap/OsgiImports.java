package com.atlassian.pocketknife.internal.querydsl.testplugin.bootstrap;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class OsgiImports {
    @ComponentImport
    EventPublisher eventPublisher;
}
